
Subforms module enables to define partial forms that can be used to edit a certain group of fields on node edit forms.

Installation
------------

Copy subforms folder to your module directory and then enable on the admin/modules page.

Usage
-----
Go to admin/structure/types/manage/TYPE/subforms to view the list of defined subforms and
admin/structure/types/manage/TYPE/subforms/add to create a new one.
On this page add a name to the new subform, select a certain group of fields which
will be used on the subform and set permissions.

Author
------
Novák Attila
96foxmulder69@gmail.com
